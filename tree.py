class Tree(object):
    def __init__(self, value, left=None, right=None):
        self.value = value
        self.left = left
        self.right = right


def level_print(tree_root):
    current_level = [tree_root]
    while current_level:
        next_level = list()
        for n in current_level:
            print n.value,
            if n.left: next_level.append(n.left)
            if n.right: next_level.append(n.right)
        print
        current_level = next_level
