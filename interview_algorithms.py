import tree as tr

def parantheses(expression):
    if len(expression) % 2 != 0:
        return False
    opened_par = set('([{')
    match = set([('(', ')'), ('[', ']'), ('{', '}')])
    stack = []
    for char in expression:
        if char in opened_par:
            stack.append(char)
        else:
            if len(stack) == 0:
                return False
            last_open = stack.pop()
            if (last_open, char) not in match:
                return False
    return len(stack) == 0

if __name__ == '__main__':
    print "First task:\n"
    test_tree = tr.Tree(1, tr.Tree(2, tr.Tree(4, tr.Tree(8)), tr.Tree(5)), tr.Tree(3, tr.Tree(6), tr.Tree(7)))
    tr.level_print(test_tree)
    print "\nSecond task:\n"
    expr = raw_input('Please enter expression:')
    if (parantheses(expr)):
        print "\nExpression is correct"
    else:
        print "\nExpression is incorrect"
